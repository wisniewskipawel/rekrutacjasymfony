
// js for datatables
$(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers",
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        "oLanguage": {
            "sSearch": "Szukaj",
        },
        "language": {
            "lengthMenu": "Pokaż _MENU_ wyników",
            "info": "Pokazuje od _START_ do _END_ z _TOTAL_ wyników",
            "paginate": {
                "first": "Pierwsza",
                "previous": "Poprzednia",
                "next": "Następna",
                "last": "Ostatnia",
            }
        },
        "columnDefs": [
            { "width": "40%", "targets": 3 }
        ],
        responsive: true
    } );
} );

// js for cookie modal
$(document).ready(function() {
    $('#cookieModal').modal('show');
});